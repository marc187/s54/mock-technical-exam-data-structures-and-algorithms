let collection = [];

// Write the queue functions below.


function print() {
    //Print queue elements.
    return collection
}

function enqueue(name) {
    //Add element to the last
    collection.push(name)
    return collection
}

function dequeue() {
    //Remove first element
    collection.shift()
    return collection
}

function front() {
    //Show first element
    return collection[0]
}

function size() {
    //Show array size
    return collection.length
}

function isEmpty() {
    //Check if array empty
    if(collection.length > 0){
        return false
    }else{
        return true
    }
}

module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};